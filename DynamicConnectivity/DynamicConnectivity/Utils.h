#pragma once
#include <istream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <set>

typedef int Vertex;

struct Edge
{
	Vertex from, to;

	inline Edge(Vertex from = -1, Vertex to = -1)
		:to(to), from(from)
	{
	}

	long long toHashLong() const
	{
		return ((1LL * from) << 32LL) + to;
	}

	long long revToHashLong() const
	{
		return ((1LL * to) << 32LL) + from;
	}
};

struct Graph
{
	int vertexCount, edgeCount;
	std::vector<std::vector<int>> vertices;
	std::vector<Edge> edges;

	inline Graph(int v, int e)
		:vertexCount(v), edgeCount(e), vertices(v, std::vector<int>()), edges(e)
	{
	}
};

class Treap;
class EulerTourTree;

class GraphInfo
{
public:
	GraphInfo() = delete;
	GraphInfo(int sizeV);

	void addNode(int x, Treap* node);
	void removeNode(int x, Treap* node);
	void addEdge(const Edge& edge, Treap* node);
	void removeEdge(const Edge& edge);
	Treap* getRandomNode(int x);
	Treap* getEdge(const Edge& edge);

private:
	std::vector<std::unordered_set<Treap*>> vert;
	std::unordered_map<long long, Treap*> edges;
};

struct Level
{
	std::vector<std::unordered_set<int>> auxEdges;
	GraphInfo* info;

	inline Level(int vertexCount)
		:auxEdges(vertexCount), info(new GraphInfo(vertexCount))
	{
	}

	inline ~Level()
	{
		if (info)
		{
			delete info;
		}
	}
};

namespace utils
{
	inline Graph readGraph(std::istream& in)
	{
		int vertexCount, edgeCount;
		in >> vertexCount >> edgeCount;
		Graph g(vertexCount, edgeCount);
		for (int i = 0; i < edgeCount; ++i)
		{
			int x, y;
			in >> x >> y;
			g.edges[i] = Edge(x, y);
			g.vertices[x].push_back(i);
		}

		return g;
	}

	class dsu
	{
	public:
		dsu(int size);
		~dsu();

		int get(int u);
		void unite(int u, int v);
	private:
		int *p, *rank;
	};
}