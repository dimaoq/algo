#pragma once
#include "Utils.h"
#include <vector>
#include <unordered_set>

class SimpleConnectivity
{
public:
	SimpleConnectivity(const Graph& g);
	
	void link(int u, int v);
	void cut(int u, int v);
	bool isConnected(int u, int v);
private:
	bool dfs(int x, int dest, std::vector<int>& used);
	std::vector<std::unordered_set<int>> vert;
};

