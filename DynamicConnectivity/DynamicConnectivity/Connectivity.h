#pragma once
#include "EulerTourTree.h"
#include "Utils.h"
#include <set>
#include <vector>
#include <unordered_map>

class Connectivity
{
public:
	Connectivity(Graph& g);
	~Connectivity();

	void link(int u, int v);
	void cut(int u, int v);
	bool isConnected(int u, int v) const;
private:
	int calcEdgeLevel(const Edge& e) const;
	void incEdgeLevel(const Edge& e);

	int levelCount;
	Level** levels;
	std::unordered_map<long long, int> edgeLevels;
};

