#include "SimpleConnectivity.h"

SimpleConnectivity::SimpleConnectivity(const Graph& g)
	:vert(g.vertexCount)
{
	for (int i = 0; i < g.edgeCount; ++i)
	{
		Edge e = g.edges[i];
		vert[e.from].insert(e.to);
		vert[e.to].insert(e.from);
	}
}

void SimpleConnectivity::link(int u, int v)
{
	vert[u].insert(v);
	vert[v].insert(u);
}

void SimpleConnectivity::cut(int u, int v)
{
	if (vert[u].count(v))
	{
		vert[u].erase(v);
		vert[v].erase(u);
	}
}

bool SimpleConnectivity::dfs(int x, int dest, std::vector<int>& used)
{
	used[x] = 1;
	if (x == dest)
	{
		return 1;
	}

	for (int i : vert[x])
	{
		if (!used[i])
		{
			int ans = dfs(i, dest, used);
			if (ans)
			{
				return 1;
			}
		}
	}

	return 0;
}

bool SimpleConnectivity::isConnected(int u, int v)
{
	std::vector<int> used(vert.size(), 0);
	return dfs(u, v, used);
}
