#include "Test.h"
#include "Utils.h"
#include "Treap.h"
#include "EulerTourTree.h"
#include "Connectivity.h"
#include "SimpleConnectivity.h"
#include <fstream>
#include <vector>
#include <chrono>
#include <iomanip>

using namespace std;

void Test::testTreap()
{
	auto graph = utils::readGraph(ifstream("in1.txt"));

	Treap* root = nullptr;
	vector<Treap*> nodes;
	for (int i = 0; i < graph.edgeCount; ++i)
	{
		auto p = Treap::insert(root, graph.edges[i], i);
		nodes.push_back(p.second);
		root = p.first;
	}

	_ASSERT(Treap::getSize(root) == graph.edgeCount);
	int size = graph.edgeCount;
	for (int i = 0; i < graph.edgeCount; ++i)
	{
		_ASSERT(nodes[i]->getParent() || nodes[i] == root);
		_ASSERT(Treap::findRoot(root) == Treap::findRoot(nodes[i]));
		_ASSERT((Treap::getKey(nodes[i])) == i);
	}

	auto p = Treap::insert(root, Edge(0, 8), size);
	root = p.first;
	_ASSERT(Treap::getSize(root) == graph.edgeCount + 1);
	size++;
	_ASSERT(Treap::findRoot(root) == Treap::findRoot(p.second));
	_ASSERT((Treap::getKey(p.second)) == size - 1);

	root = Treap::remove(root, 1); //1 2
	for (int i = 2; i < size - 1; ++i)
	{
		_ASSERT(Treap::findRoot(nodes[i]) == Treap::findRoot(p.second));
	}
}

void testConnectedAll(Level& level, int st, int x)
{
	for (int i = st; i < x; ++i)
	{
		for (int j = st; j < x; ++j)
		{
			if (i == j) continue;
			_ASSERT(EulerTourTree::isConnected(level, i, j));
		}
	}
}

void Test::testEulerTree()
{
	auto graph = utils::readGraph(ifstream("in1.txt"));

	Level level(graph.vertexCount);
	int* bu = new int[graph.vertexCount];
	memset(bu, 0, sizeof(int) * graph.vertexCount);
	auto root = EulerTourTree::build(level, graph, 1, bu);
	delete[] bu;

	_ASSERT(Treap::getSize(root) == graph.edgeCount * 2);
	testConnectedAll(level, 0, graph.vertexCount);
	auto r = EulerTourTree::cut(level, 2, 3);
	testConnectedAll(level, 3, 6);
	for (int i = 0; i < graph.vertexCount; ++i)
	{
		for (int j = 0; j < graph.vertexCount; ++j)
		{
			if (i == j) continue;
			if (i > 2 && i < 6) continue;
			if (j > 2 && j < 6) continue;
			_ASSERT(EulerTourTree::isConnected(level, i, j));
		}
	}
	for (int i = 0; i < graph.vertexCount; ++i)
	{
		for (int j = 3; j < 6; ++j)
		{
			if (i == j) continue;
			if (i > 2 && i < 6) continue;
			_ASSERT(!EulerTourTree::isConnected(level, i, j));
		}
	}
	root = EulerTourTree::link(level, 2, 3);
	testConnectedAll(level, 0, graph.vertexCount);
	EulerTourTree::cut(level, 2, 3);
	EulerTourTree::cut(level, 2, 6);
	_ASSERT(!EulerTourTree::isConnected(level, 3, 0));
	_ASSERT(!EulerTourTree::isConnected(level, 7, 4));
	_ASSERT(!EulerTourTree::isConnected(level, 2, 6));
	_ASSERT(EulerTourTree::isConnected(level, 5, 4));
	_ASSERT(EulerTourTree::isConnected(level, 6, 7));
	EulerTourTree::link(level, 3, 6);
	_ASSERT(EulerTourTree::isConnected(level, 6, 4));
	_ASSERT(EulerTourTree::isConnected(level, 7, 3));
	_ASSERT(EulerTourTree::isConnected(level, 6, 7));
	_ASSERT(EulerTourTree::isConnected(level, 7, 5));
	EulerTourTree::cut(level, 6, 7);
	_ASSERT(!EulerTourTree::isConnected(level, 6, 7));
	_ASSERT(!EulerTourTree::isConnected(level, 7, 5));
	/*char c;
	scanf("%c", &c);*/
}

void testConnAll(Connectivity& cn, int from, int to)
{
	for (int i = from; i < to; ++i)
	{
		for (int j = from; j < to; ++j)
		{
			_ASSERT(cn.isConnected(i, j));
		}
	}
}

void Test::testConnectivity()
{
	auto graph = utils::readGraph(ifstream("in1.txt"));

	Connectivity conn(graph);
	SimpleConnectivity simple(graph);

	testConnAll(conn, 0, graph.vertexCount);
	for (int i = 0; i < graph.vertexCount; ++i)
	{
		for (int j = 0; j < graph.vertexCount; ++j)
		{
			_ASSERT(simple.isConnected(i, j));
		}
	}

	conn.link(0, 4);
	simple.link(0, 4);
	_ASSERT(conn.isConnected(1, 4));
	_ASSERT(conn.isConnected(0, 4));
	_ASSERT(conn.isConnected(2, 0));
	_ASSERT(simple.isConnected(1, 4));
	_ASSERT(simple.isConnected(0, 4));
	_ASSERT(simple.isConnected(2, 0));

	conn.cut(1, 2);
	simple.cut(1, 2);
	_ASSERT(conn.isConnected(1, 4));
	_ASSERT(conn.isConnected(0, 4));
	_ASSERT(conn.isConnected(2, 0));
	_ASSERT(simple.isConnected(1, 4));
	_ASSERT(simple.isConnected(0, 4));
	_ASSERT(simple.isConnected(2, 0));
	conn.cut(1, 0);
	simple.cut(1, 0);
	_ASSERT(!conn.isConnected(1, 4));
	_ASSERT(conn.isConnected(0, 4));
	_ASSERT(conn.isConnected(2, 0));
	_ASSERT(!conn.isConnected(1, 6));
	_ASSERT(!simple.isConnected(1, 4));
	_ASSERT(simple.isConnected(0, 4));
	_ASSERT(simple.isConnected(2, 0));
	_ASSERT(!simple.isConnected(1, 6));

	conn.link(1, 0);
	_ASSERT(conn.isConnected(1, 4));
	_ASSERT(conn.isConnected(0, 4));
	_ASSERT(conn.isConnected(2, 0));

	conn.cut(2, 3);
	_ASSERT(conn.isConnected(1, 5));
	_ASSERT(!conn.isConnected(0, 2));
	_ASSERT(!conn.isConnected(4, 7));
	_ASSERT(conn.isConnected(4, 5));

	conn.cut(3, 4);
	_ASSERT(!conn.isConnected(1, 5));
	_ASSERT(!conn.isConnected(5, 7));
	_ASSERT(!conn.isConnected(4, 3));
	_ASSERT(conn.isConnected(3, 5));

	conn.cut(0, 4);
	conn.link(0, 5);

	_ASSERT(conn.isConnected(1, 5));
	_ASSERT(!conn.isConnected(5, 7));
	_ASSERT(!conn.isConnected(4, 1));
	_ASSERT(conn.isConnected(3, 1));

	conn.link(5, 7);
	conn.link(5, 6);
	conn.link(1, 5);
	_ASSERT(conn.isConnected(0, 6));
	_ASSERT(conn.isConnected(1, 7));
	_ASSERT(conn.isConnected(0, 7));
	_ASSERT(!conn.isConnected(4, 3));

	conn.cut(5, 1);
	_ASSERT(conn.isConnected(0, 6));
	_ASSERT(conn.isConnected(1, 7));
	_ASSERT(conn.isConnected(0, 7));
	_ASSERT(!conn.isConnected(4, 3));

	conn.cut(5, 6);
	conn.cut(5, 7);
	_ASSERT(conn.isConnected(2, 6));
	_ASSERT(conn.isConnected(6, 7));
	_ASSERT(conn.isConnected(2, 7));
	_ASSERT(!conn.isConnected(2, 3));
	_ASSERT(!conn.isConnected(6, 0));
	_ASSERT(!conn.isConnected(4, 7));
}

void check(const Graph& graph, Connectivity& test, SimpleConnectivity &simple)
{
	for (int i = 0; i < graph.vertexCount; ++i)
	{
		for (int j = i + 1; j < graph.vertexCount; ++j)
		{
			bool a = test.isConnected(i, j);
			_ASSERT(simple.isConnected(i, j) == a);
		}
	}
}

void makeLink(Connectivity& test, SimpleConnectivity &simple, int u, int v)
{
	printf("link %d %d\n", u, v);
	simple.link(u, v);
	test.link(u, v);
}

void makeCut(Connectivity& test, SimpleConnectivity &simple, int u, int v)
{
	printf("cut %d %d\n", u, v);
	simple.cut(u, v);
	test.cut(u, v);
}

void traceNode(Treap* t)
{
	auto f = Treap::getFirst(t);
	while (f)
	{
		auto e = f->getValue();
		printf("%d-%d ", e.from, e.to);
		f = Treap::getNext(t);
	}
	printf("\n");
}

void Test::compareTest(std::string& s)
{
	auto graph = utils::readGraph(ifstream(s));
	Connectivity test(graph);
	SimpleConnectivity simple(graph);

	check(graph, test, simple);

	int c = 0;
	for (int i = 0; i < 1000; ++i)
	{
		int u = rand() % graph.vertexCount;
		int v = rand() % graph.vertexCount;
		while (v == u)
		{
			v = rand() % graph.vertexCount;
		}

		int act = rand() % 10;
		if (act < 5) //link
		{
			simple.link(u, v);
			test.link(u, v);
		}
		else
		{
			simple.cut(u, v);
			test.cut(u, v);
			c++;
		}
		
		check(graph, test, simple);
	}
}

long long findAllConnectivity(Connectivity& conn, SimpleConnectivity& simple, const Graph& g, int ind)
{
	const long long maxOptime = 4LL * 1000 * 1000 * 1000;
	const long long maxtime = 15 * maxOptime;
	long long res = 0LL;
	auto start = chrono::high_resolution_clock::now();
	for (int i = 0; i < g.vertexCount; ++i)
	{
		for (int j = i + 1; j < g.vertexCount; ++j)
		{
			auto startOp = chrono::high_resolution_clock::now();
			bool a = ind ? conn.isConnected(i, j) : simple.isConnected(i, j);
			auto elapsed = chrono::high_resolution_clock::now() - startOp;

			if (res > maxtime || elapsed.count() > maxOptime)
			{
				return -1;
			}
			res += elapsed.count();
		}
	}

	return res;
}

pair<long long, long long> randomOperation(Connectivity& conn, SimpleConnectivity& simple, const Graph& g, int queryCount)
{
	const long long maxOptime = 4LL * 1000 * 1000 * 1000;
	const long long maxtime = 15 * maxOptime;
	long long resSimple = 0LL, resFast = 0LL;
	int probCheck = 0, probLink = 5, probCut = 5;
	for (int i = 0; i < queryCount; ++i)
	{
		int act = rand() % (probCheck + probLink + probCut);
		int u = rand() % g.vertexCount;
		int v = rand() % g.vertexCount;
		while (v == u)
		{
			v = rand() % g.vertexCount;
		}

		if (act < probCut)
		{
			if (resSimple != -1)
			{
				auto startOp = chrono::high_resolution_clock::now();
				simple.cut(u, v);
				auto elapsed = chrono::high_resolution_clock::now() - startOp;
				if (resSimple > maxtime || elapsed.count() > maxOptime)
				{
					resSimple = -1;
				}
				else
				{
					resSimple += elapsed.count();
				}
			}

			if (resFast != -1)
			{
				auto startOp = chrono::high_resolution_clock::now();
				conn.cut(u, v);
				auto elapsed = chrono::high_resolution_clock::now() - startOp;
				if (resFast > maxtime || elapsed.count() > maxOptime)
				{
					resFast = -1;
				}
				else
				{
					resFast += elapsed.count();
				}
			}
		}
		else
		{
			if (act < probCut + probCheck)
			{
				if (resSimple != -1)
				{
					auto startOp = chrono::high_resolution_clock::now();
					bool q = simple.isConnected(u, v);
					auto elapsed = chrono::high_resolution_clock::now() - startOp;
					if (resSimple > maxtime || elapsed.count() > maxOptime)
					{
						resSimple = -1;
					}
					else
					{
						resSimple += elapsed.count();
					}
				}

				if (resFast != -1)
				{
					auto startOp = chrono::high_resolution_clock::now();
					bool w = conn.isConnected(u, v);
					auto elapsed = chrono::high_resolution_clock::now() - startOp;
					if (resFast > maxtime || elapsed.count() > maxOptime)
					{
						resFast = -1;
					}
					else
					{
						resFast += elapsed.count();
					}
				}
			}
			else
			{
				if (resSimple != -1)
				{
					auto startOp = chrono::high_resolution_clock::now();
					simple.link(u, v);
					auto elapsed = chrono::high_resolution_clock::now() - startOp;
					if (resSimple > maxtime || elapsed.count() > maxOptime)
					{
						resSimple = -1;
					}
					else
					{
						resSimple += elapsed.count();
					}
				}

				if (resFast != -1)
				{
					auto startOp = chrono::high_resolution_clock::now();
					conn.link(u, v);
					auto elapsed = chrono::high_resolution_clock::now() - startOp;
					if (resFast > maxtime || elapsed.count() > maxOptime)
					{
						resFast = -1;
					}
					else
					{
						resFast += elapsed.count();
					}
				}
			}
		}
	}

	return make_pair(resFast, resSimple);
}

void Test::makeBenchmark(int fileEnd, int secEnd)
{
	ofstream fast("benchfast3.txt"), simple("benchsimple3.txt");
	fast << fixed << setprecision(3);
	simple << fixed << setprecision(3);
	for (int i = 5; i < fileEnd; ++i)
	{
		for (int j = 0; j < secEnd; ++j)
		{
			printf("run %d %d\n", i, j);
			auto g = utils::readGraph(ifstream("test" + to_string(i) + to_string(j) + ".txt"));
			
			Connectivity test(g);
			SimpleConnectivity csimple(g);

			auto tfast = findAllConnectivity(test, csimple, g, 1);
			auto tsimple = i > 3 ? -1LL : findAllConnectivity(test, csimple, g, 0);
			fast << (tfast == -1 ? -1 : tfast / (1000.0 * 1000)) << " ";
			simple << (tsimple == -1 ? -1 : tsimple / (1000.0 * 1000)) << " ";
		}
		fast << endl;
		simple << endl;
	}
}

void Test::benchRandomOperation(int fileEnd, int secEnd)
{
	ofstream fast("randFast.txt"), simple("randSimple.txt");
	fast << fixed << setprecision(3);
	simple << fixed << setprecision(3);
	for (int i = 0; i < fileEnd; ++i)
	{
		auto g = utils::readGraph(ifstream("test" + to_string(i) + to_string(secEnd) + ".txt"));

		Connectivity test(g);
		SimpleConnectivity csimple(g);

		auto res = randomOperation(test, csimple, g, 50000);
		auto tfast = res.first, tsimple = res.second;
		fast << (tfast == -1 ? -1 : tfast / (1000.0 * 1000)) << endl;
		simple << (tsimple == -1 ? -1 : tsimple / (1000.0 * 1000)) << endl;
	}
}