#include "Connectivity.h"
#include "Treap.h"
#include "Utils.h"
#include <unordered_set>

using namespace std;

void buildSpanningTree(const Graph& g, vector<int>& result)
{
	utils::dsu dsu(g.vertexCount);
	for (int i = 0; i < g.edgeCount; ++i)
	{
		const Edge& e = g.edges[i];
		if (dsu.get(e.from) != dsu.get(e.to))
		{
			result.push_back(i);
			dsu.unite(e.from, e.to);
		}
	}
}

Connectivity::Connectivity(Graph& g)
	:levels(nullptr)
{
	int log = ilogb(g.vertexCount);
	if (g.vertexCount != (1 << log))
	{
		log++;
	}

	levelCount = log;
	levels = new Level*[levelCount];
	for (int i = 0; i < log; ++i)
	{
		levels[i] = new Level(g.vertexCount);
	}
	
	vector<int> spanIndex;
	buildSpanningTree(g, spanIndex);

	Graph spanGraph(g.vertexCount, (int)spanIndex.size());
	for (int i = 0; i < (int)spanIndex.size(); ++i)
	{
		Edge was = g.edges[spanIndex[i]];
		spanGraph.edges[i] = was;
	}

	int* usedVertices = new int[g.vertexCount];
	memset(usedVertices, 0, sizeof(int) * g.vertexCount);
	for (int i = 0; i < g.vertexCount; ++i)
	{
		if (!usedVertices[i])
		{
			EulerTourTree::build(*levels[0], spanGraph, i, usedVertices);
		}
	}
	delete[] usedVertices;

	vector<char> auxEdges(g.edgeCount, 0);
	for (int i = 0; i < (int)spanIndex.size(); ++i)
	{
		auxEdges[spanIndex[i]] = 1;
	}
	for (int i = 0; i < g.edgeCount; ++i)
	{
		Edge& e = g.edges[i];
		if (auxEdges[i] == 0)
		{
			levels[0]->auxEdges[e.from].insert(e.to);
			levels[0]->auxEdges[e.to].insert(e.from);
		}

		edgeLevels[e.toHashLong()] = 0;
		edgeLevels[e.revToHashLong()] = 0;
	}
}

Connectivity::~Connectivity()
{
	if (levels)
	{
		for (int i = 0; i < levelCount; ++i)
		{
			delete levels[i];
		}
		delete levels;
	}
}

int Connectivity::calcEdgeLevel(const Edge& e) const
{
	auto has = edgeLevels.find(e.toHashLong());
	if (has != edgeLevels.cend())
	{
		return has->second;
	}

	return -1;
}

void Connectivity::incEdgeLevel(const Edge& e)
{
	edgeLevels[e.toHashLong()]++;
	edgeLevels[e.revToHashLong()]++;
}

void Connectivity::link(int u, int v)
{
	Edge e(u, v);
	if (calcEdgeLevel(e) != -1)
	{
		return;
	}

	if (!isConnected(u, v))
	{
		EulerTourTree::link(*levels[0], u, v);
	}
	else
	{
		levels[0]->auxEdges[u].insert(v);
		levels[0]->auxEdges[v].insert(u);
	}
	edgeLevels[e.toHashLong()] = 0;
	edgeLevels[e.revToHashLong()] = 0;
}

void Connectivity::cut(int u, int v)
{
	int level;
	Edge e(u, v);
	if ((level = calcEdgeLevel(e)) == -1)
	{
		return;
	}

	edgeLevels.erase(e.toHashLong());
	edgeLevels.erase(e.revToHashLong());

	if (levels[0]->auxEdges[u].count(v))
	{
		//printf("delete aux edge\n");
		for (int i = level; i >= 0; --i)
		{
			levels[i]->auxEdges[u].erase(v);
			levels[i]->auxEdges[v].erase(u);
		}
	}
	else
	{
		bool found = false;
		int foundLevel = -1;
		Edge replacementEdge;
		for (int i = level; i >= 0; --i)
		{
			auto cut = EulerTourTree::cut(*levels[i], u, v);
			_ASSERT(!EulerTourTree::isConnected(*levels[i], u, v));
			auto rootU = Treap::findRoot(levels[i]->info->getRandomNode(u)), 
				 rootV = Treap::findRoot(levels[i]->info->getRandomNode(v));
			_ASSERT((rootU != rootV) || (rootU == nullptr));
			
			if (!found)
			{
				if (Treap::getSize(rootU) > Treap::getSize(rootV))
				{
					swap(u, v);
					swap(rootU, rootV);
				}

				unordered_set<int> uVertices;
				if (rootU == nullptr)
				{
					uVertices.insert(u);
				}
				else
				{
					Treap* it = Treap::getFirst(rootU);
					while (it)
					{
						Edge e = it->getValue();
						if (calcEdgeLevel(e) == i)
						{
							incEdgeLevel(e);
							EulerTourTree::link(*levels[i + 1], e.from, e.to);
						}

						uVertices.insert(e.from);
						uVertices.insert(e.to);

						it = Treap::getNext(rootU);
					}
				}

				for (auto uvert : uVertices)
				{
					if (found) break;
					auto& auxUEdges = levels[i]->auxEdges[uvert];
					for (auto vAux : auxUEdges)
					{
						Edge e(uvert, vAux);
						if (calcEdgeLevel(e) == i)
						{
							if (Treap::findRoot(levels[i]->info->getRandomNode(vAux)) == rootV)
							{
								//replacement edge (uvert, vAux)
								replacementEdge = e;
								//printf("%d %d %d\n", replacementEdge.from, replacementEdge.to, i);
								_ASSERT(!EulerTourTree::isConnected(*levels[i], uvert, vAux));
								EulerTourTree::link(*levels[i], uvert, vAux);
								levels[i]->auxEdges[uvert].erase(vAux);
								levels[i]->auxEdges[vAux].erase(uvert);
							
								found = true;
								break;
							}
							else
							{
								incEdgeLevel(e);
								levels[i + 1]->auxEdges[uvert].insert(vAux);
								levels[i + 1]->auxEdges[vAux].insert(uvert);
							}
						}
					}
				}
			}
			else
			{
				//printf("%d %d\n", replacementEdge.from, replacementEdge.to);
				EulerTourTree::link(*levels[i], replacementEdge.from, replacementEdge.to);
				//_ASSERT(levels[i]->auxEdges[replacementEdge.from].count(replacementEdge.to));
				//_ASSERT(levels[i]->auxEdges[replacementEdge.to].count(replacementEdge.from));
				levels[i]->auxEdges[replacementEdge.from].erase(replacementEdge.to);
				levels[i]->auxEdges[replacementEdge.to].erase(replacementEdge.from);
			}
		}
	}
}

bool Connectivity::isConnected(int u, int v) const
{
	return EulerTourTree::isConnected(*levels[0], u, v);
}