#pragma once
#include <string>

class Test
{
public:
	static void testTreap();
	static void testEulerTree();
	static void testConnectivity();
	static void compareTest(std::string& s);
	static void makeBenchmark(int fileEnd, int secEnd);
	static void benchRandomOperation(int fileEnd, int secEnd);
};

