#pragma once
#include "Utils.h"
#include <algorithm>
#include <stack>

class Treap
{
public:
	Treap(int y, Edge value);
	~Treap();

	Edge getValue() const;
	Treap* getParent() const;

	static std::pair<Treap*, Treap*> insert(Treap* t, const Edge& edge, int pos);
	static Treap* remove(Treap* t, int key);

	static std::pair<Treap*, Treap*> split(Treap* t, int key);
	static Treap* merge(Treap* left, Treap* right);

	static Treap* findRoot(Treap* t);
	static int getKey(Treap* t);
	static int getSize(Treap* t);
	
	static Treap* getFirst(Treap* t);
	static Treap* getNext(Treap* t);
private:
	static int count(Treap* t);
	static void updateCnt(Treap* t);

	int cnt, y;
	Edge value;
	Treap *left, *right, *parent;

	std::stack<Treap*> iteration;
};

