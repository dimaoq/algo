#include "Treap.h"

Treap::Treap(int y, Edge value)
	:cnt(1), y(y), value(std::move(value)), left(nullptr), right(nullptr), parent(nullptr), iteration()
{
}

Treap::~Treap()
{
	if (left)
	{
		delete left;
	}

	if (right)
	{
		delete right;
	}
}

std::pair<Treap*, Treap*> Treap::insert(Treap* t, const Edge& edge, int pos)
{
	auto splitted = split(t, pos);
	auto newNode = new Treap((rand() << 16) | rand(), edge);
	return std::make_pair(merge(merge(splitted.first, newNode), splitted.second), newNode);
}

Treap* Treap::remove(Treap* t, int key)
{
	auto p = split(t, key);
	auto p2 = split(p.second, 1);
	delete p2.first;
	return merge(p.first, p2.second);
}

std::pair<Treap*, Treap*> Treap::split(Treap* t, int key)
{
	if (!t)
	{
		return std::make_pair(nullptr, nullptr);
	}

	int rootPos = count(t->left);
	if (key <= rootPos)
	{
		auto splitted = split(t->left, key);
		t->left = splitted.second;
		updateCnt(t);
		return std::make_pair(splitted.first, t);
	}
	else
	{
		auto splitted = split(t->right, key - rootPos - 1);
		t->right = splitted.first;
		updateCnt(t);
		return std::make_pair(t, splitted.second);
	}
}

Treap* Treap::merge(Treap* left, Treap* right)
{
	if (!left || !right)
	{
		auto ret = left ? left : right;
		return ret;
	}
	else if (left->y < right->y)
	{
		left->right = merge(left->right, right);
		updateCnt(left);
		return left;
	}
	else
	{
		right->left = merge(left, right->left);
		updateCnt(right);
		return right;
	}
}

Treap* Treap::findRoot(Treap* t)
{
	if (!t) return nullptr;
	
	while (t->parent)
	{
		t = t->parent;
	}
	return t;
}

int Treap::getKey(Treap* t)
{
	if (!t) return -1;

	int res = count(t->left);
	while (t->parent)
	{
		if (t->parent->right == t)
		{
			res += count(t->parent->left) + 1;
		}
		t = t->parent;
	}

	return res;
}

int Treap::getSize(Treap* t)
{
	return count(t);
}

Edge Treap::getValue() const
{
	return value;
}

Treap* Treap::getParent() const
{
	return parent;
}

int Treap::count(Treap* t)
{
	return t ? t->cnt : 0;
}

void Treap::updateCnt(Treap* t)
{
	if (t)
	{
		t->cnt = count(t->right) + count(t->left) + 1;

		if (t->left)
		{
			t->left->parent = t;
		}

		if (t->right)
		{
			t->right->parent = t;
		}

		t->parent = nullptr;
	}
}

Treap* Treap::getFirst(Treap* t)
{
	t->iteration = std::stack<Treap*>();
	if (t->left)
		t->iteration.push(t->left);
	if (t->right)
		t->iteration.push(t->right);

	return t;
}

Treap* Treap::getNext(Treap* t)
{
	if (t->iteration.empty())
	{
		return nullptr;
	}
	
	Treap* cur = t->iteration.top();
	t->iteration.pop();
	
	if (cur->left)
		t->iteration.push(cur->left);
	if (cur->right)
		t->iteration.push(cur->right);
	
	return cur;
}
