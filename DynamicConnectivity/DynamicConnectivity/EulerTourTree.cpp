#include "EulerTourTree.h"
#include "Treap.h"

using namespace std;

void dfs(const Graph& g, int x, std::vector<int>& edgeIndex, std::vector<char>& used, int p, int* usedVert)
{
	int backEdge = p == -1 ? -1 : p - 2 * (p & 1) + 1;
	for (size_t i = 0; i < g.vertices[x].size(); ++i)
	{
		int edge = g.vertices[x][i];
		if (backEdge != edge && !used[edge])
		{
			edgeIndex.push_back(edge);
			used[edge] = 1;
			usedVert[g.edges[edge].to] = 1;
			usedVert[g.edges[edge].from] = 1;
			dfs(g, g.edges[edge].to, edgeIndex, used, edge, usedVert);
		}
	}

	if (backEdge != -1)
	{
		edgeIndex.push_back(backEdge);
		used[backEdge] = 1;
		usedVert[g.edges[backEdge].to] = 1;
		usedVert[g.edges[backEdge].from] = 1;
	}
}

Treap* EulerTourTree::build(Level& level, const Graph& g, int start, int* usedVert)
{
	Graph oriented(g.vertexCount, g.edgeCount * 2);
	for (int i = 0; i < g.edgeCount; ++i)
	{
		Edge e = g.edges[i];
		
		oriented.edges[i << 1] = e;
		oriented.edges[(i << 1) + 1] = Edge(e.to, e.from);

		oriented.vertices[e.from].push_back(i << 1);
		oriented.vertices[e.to].push_back((i << 1) + 1);
	}
	vector<int> edges;
	vector<char> used(g.edgeCount * 2, 0);
	dfs(oriented, start, edges, used, -1, usedVert);

	Treap* root = nullptr;
	for (int i = 0; i < (int)edges.size(); ++i)
	{
		//printf("%d-%d ", g.edges[edges[i]].from, g.edges[edges[i]].to);
		auto& edge = oriented.edges[edges[i]];
		auto p = Treap::insert(root, edge, Treap::getSize(root));
		level.info->addNode(edge.from, p.second);
		level.info->addEdge(edge, p.second);
		root = p.first;
	}

	return root;
}

std::pair<Treap*, Treap*> EulerTourTree::cut(Level& level, int u, int v)
{
	Edge edgeUV = Edge(u, v), edgeVU = Edge(v, u);
	auto uv = level.info->getEdge(edgeUV), vu = level.info->getEdge(edgeVU);

	auto root = Treap::findRoot(uv), root2 = Treap::findRoot(vu);
	_ASSERT(root && root == root2);

	int keyU = Treap::getKey(uv), keyV = Treap::getKey(vu);
	if (keyU > keyV)
	{
		std::swap(u, v);
		std::swap(edgeUV, edgeVU);
		std::swap(uv, vu);
		std::swap(keyU, keyV);
	}

	level.info->removeNode(u, uv);
	level.info->removeNode(v, vu);
	level.info->removeEdge(edgeUV);
	level.info->removeEdge(edgeVU);

	auto splitU = Treap::split(root, keyU);
	splitU.second = Treap::remove(splitU.second, 0);
	auto splitV = Treap::split(splitU.second, keyV - keyU - 1);
	splitV.second = Treap::remove(splitV.second, 0);
	Treap* left = splitU.first, *middle = splitV.first, *right = splitV.second;

	return std::make_pair(Treap::merge(left, right), middle);
}

Treap* EulerTourTree::link(Level& level, int u, int v)
{
	Treap *fu = level.info->getRandomNode(u), *fv = level.info->getRandomNode(v);
	auto rootU = Treap::findRoot(fu), rootV = Treap::findRoot(fv);
	_ASSERT(!rootU || rootU != rootV);
	Edge edgeUV = Edge(u, v), edgeVU = Edge(v, u);

	auto splitU = Treap::split(rootU, Treap::getKey(fu));
	auto splitV = Treap::split(rootV, Treap::getKey(fv));
	auto m1 = Treap::insert(splitU.first, edgeUV, Treap::getSize(splitU.first));
	auto m2 = Treap::insert(splitV.first, edgeVU, Treap::getSize(splitV.first));
	
	level.info->addNode(u, m1.second);
	level.info->addNode(v, m2.second);
	level.info->addEdge(edgeUV, m1.second);
	level.info->addEdge(edgeVU, m2.second);

	return Treap::merge(Treap::merge(m1.first, splitV.second), Treap::merge(m2.first, splitU.second));
}

bool EulerTourTree::isConnected(const Level& level, int u, int v)
{
	auto root = Treap::findRoot(level.info->getRandomNode(u));
	if (!root) return u == v;
	return root == Treap::findRoot(level.info->getRandomNode(v));
}