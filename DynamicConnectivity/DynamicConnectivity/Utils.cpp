#include "Utils.h"

GraphInfo::GraphInfo(int sizeV)
	:vert(sizeV, std::unordered_set<Treap*>())
{
}

void GraphInfo::addNode(int x, Treap* node)
{
	vert[x].insert(node);
}

void GraphInfo::removeNode(int x, Treap* node)
{
	vert[x].erase(node);
}

void GraphInfo::addEdge(const Edge& edge, Treap* node)
{
	edges[edge.toHashLong()] = node;
}

void GraphInfo::removeEdge(const Edge& edge)
{
	edges.erase(edge.toHashLong());
}

Treap* GraphInfo::getRandomNode(int x)
{
	auto& s = vert[x];
	if (s.empty())
	{
		return nullptr;
	}

	return *(s.begin());
}

Treap* GraphInfo::getEdge(const Edge& edge)
{
	return edges[edge.toHashLong()];
}

using namespace utils;

dsu::dsu(int size)
	:p(new int[size]), rank(new int[size])
{
	for (int i = 0; i < size; ++i)
	{
		p[i] = i;
		rank[i] = 0;
	}
}

dsu::~dsu()
{
	if (p)
	{
		delete[] p;
	}

	if (rank)
	{
		delete[] rank;
	}
}

int dsu::get(int u)
{
	return (u == p[u]) ? u : (p[u] = get(p[u]));
}

void dsu::unite(int u, int v)
{
	int a = get(u), b = get(v);
	if (a != b)
	{
		if (rank[a] < rank[b])
		{
			std::swap(a, b);
		}
		p[b] = a;
		
		if (rank[a] == rank[b])
		{
			++rank[a];
		}
	}
}