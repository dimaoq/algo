#pragma once
#include "Utils.h"
#include <vector>

class EulerTourTree
{
public:
	static Treap* build(Level& level, const Graph& g, int root, int* usedVert);
	static std::pair<Treap*, Treap*> cut(Level& level, int u, int v);
	static Treap* link(Level& level, int u, int v);
	static bool isConnected(const Level& level, int u, int v);
};

