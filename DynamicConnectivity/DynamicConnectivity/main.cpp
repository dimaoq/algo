#include <algorithm>
#include "Test.h"
#include <ctime>
#include <string>
#include <fstream>
#include <vector>
#include <set>

using namespace std;

const int SHUFFLE_COUNT = 1;

void genTestGraph(std::string& file, pair<int, int>* edges, int vcount, int ecount)
{
	ofstream fout(file.c_str());
	fout << vcount << " " << ecount << endl;
	int allCount = int(1LL * vcount * (vcount - 1) / 2);
	
	for (int i = 0; i < SHUFFLE_COUNT; ++i)
	{
		random_shuffle(edges, edges + allCount);
	}

	for (int i = 0; i < ecount; ++i)
	{
		fout << edges[i].first << " " << edges[i].second << endl;
	}
}

void genTests()
{
	int vCount = 50;
	for (int i = 0; ; ++i)
	{
		if (vCount >= 5001)
		{
			break;
		}

		long long maxE = 1LL * vCount * (vCount - 1) / 2;
		pair<int, int>* edges = new pair<int, int>[maxE];
		int st = 0;
		for (int i = 0; i < vCount - 1; ++i)
		{
			for (int j = i + 1; j < vCount; ++j)
			{
				edges[st++] = make_pair(i, j);
			}
		}

		genTestGraph("test" + to_string(i) + "0.txt", edges, vCount, min((int)maxE, vCount * 2));
		genTestGraph("test" + to_string(i) + "1.txt", edges, vCount, min((int)maxE, vCount * 5));
		genTestGraph("test" + to_string(i) + "2.txt", edges, vCount, maxE / 50);
		genTestGraph("test" + to_string(i) + "3.txt", edges, vCount, maxE / 25);
		genTestGraph("test" + to_string(i) + "4.txt", edges, vCount, maxE / 10);

		delete[] edges;

		vCount *= (i & 1) ? 5 : 2;
	}
}

int main()
{
	srand(time(0));
#ifdef _DEBUG
	Test::testTreap();
	Test::testEulerTree();
	Test::testConnectivity();
	Test::compareTest(string("in2.txt"));
#endif
	//genTests();
	//Test::makeBenchmark(6, 5);
	Test::benchRandomOperation(6, 4);

	return 0;
}